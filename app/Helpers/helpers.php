<?php

if(!function_exists('isMd5')){
    function isMd5(string $key)
    {
        return preg_match('/^[a-f0-9]{32}$/', $key);
    }
}
