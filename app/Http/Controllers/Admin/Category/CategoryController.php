<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\CategoryIndexRequest;
use App\Http\Requests\Admin\Category\CategoryStoreRequest;
use App\Http\Requests\Admin\Category\CategoryUpdateRequest;
use App\Models\Category;
use App\Services\Category\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * @param CategoryIndexRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(CategoryIndexRequest $request)
    {
        $categories = Category::with('parent')
            ->latest()
            ->paginate();
        return view('admin.category.index', compact('categories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $parents = Category::query()->whereNull('parent_id')->get();
        return view('admin.category.create', compact('parents'));
    }

    /**
     * @param CategoryStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryStoreRequest $request)
    {
        $categoryService = CategoryService::new();
        $categoryService->afterCallback(function (Category $category) use ($request) {
            $category->parent()->associate($request->get('parent_id'));
        });
        $categoryService->create($request->only('slug'));

        return redirect()->route('admin.categories.index');
    }

    /**
     * @param $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($category)
    {
        $parents = Category::query()
            ->whereNull('parent_id')
            ->where('id', '!=',  $category->id)
            ->get();

        return view('admin.category.edit', compact('category','parents'));
    }

    /**
     * @param CategoryUpdateRequest $request
     * @param $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryUpdateRequest $request, $category)
    {
        $categoryService = CategoryService::make($category);
        $categoryService->afterCallback(function (Category $category) use ($request) {
            $category->parent()->associate($request->get('parent_id'));
        });
        $categoryService->update($request->only('slug'));

        return redirect()->route('admin.categories.index');
    }

    /**
     * @param Request $request
     * @param $category
     * @return \Illuminate\Http\RedirectResponse|Response
     */
    public function destroy(Request $request, $category)
    {
        if ($request->wantsJson()) {
            CategoryService::make($category)->delete();
            return response()->noContent(Response::HTTP_NO_CONTENT);
        } else {
            return redirect()->route('user.categories.index');
        }

    }
}
