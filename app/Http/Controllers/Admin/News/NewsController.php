<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\News\NewsStoreRequest;
use App\Http\Requests\Admin\News\NewsUpdateRequest;
use App\Models\Category;
use App\Models\News;
use App\Models\Status;
use App\Services\News\NewsService;
use App\Services\Tag\TagService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Response;

class NewsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $news = News::with(['status'])->latest()->paginate();
        return view('admin.news.index', compact('news'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::with('childrenRecursive')
            ->whereNull('parent_id')
            ->get();

        $statuses = Cache::rememberForever('statuses', function () {
            return Status::whereIn('slug', ['published', 'draft'])->get();
        });
        return view('admin.news.create', compact('categories', 'statuses'));
    }

    /**
     * @param NewsStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NewsStoreRequest $request)
    {
        $inputs = $request->only('title', 'external_url', 'summary', 'body', 'status_id', 'category_id', 'tags');
        $newsService = NewsService::new();
        $newsService->afterCallback(function (News $news) use ($inputs) {
            $news->status()->associate($inputs['status_id']);
            $news->category()->associate($inputs['category_id']);

            collect($inputs['tags'])->map(function ($tag) use (&$tags){
                $tags[]['slug'] = $tag;
            });
            if($inputs['tags']) {
                $tagService = TagService::new();
                $tagService = $tagService->firstOrCreateMany($tags);
                $news->tags()->sync($tagService);
            }

        });
        $newsService->create($inputs);

        return redirect()->route('admin.news.index');
    }


    /**
     * @param $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($news)
    {
        $categories = Category::with('childrenRecursive')
            ->whereNull('parent_id')
            ->get();

        $statuses = Cache::rememberForever('statuses', function () {
            return Status::whereIn('slug', ['published', 'draft'])->get();
        });

        return view('admin.news.edit', compact('news', 'statuses', 'categories'));
    }

    /**
     * @param NewsUpdateRequest $request
     * @param $news
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NewsUpdateRequest $request, $news)
    {
        $inputs = $request->only('title', 'external_url', 'summary', 'body', 'status_id', 'category_id', 'tags');
        $newsService = NewsService::make($news);
        $newsService->afterCallback(function (News $news) use ($inputs) {

            $news->status()->associate($inputs['status_id']);
            $news->category()->associate($inputs['category_id']);


            collect($inputs['tags'])->map(function ($tag) use (&$tags){
                $tags[]['slug'] = $tag;
            });

            if($inputs['tags']) {
                $tagService = TagService::new();
                $tagService = $tagService->firstOrCreateMany($tags);
                $news->tags()->sync($tagService);
            }else {
                $news->tags()->detach();
            }

        });
        $newsService->update($inputs);

        return redirect()->route('admin.news.index');
    }

    /**
     * @param Request $request
     * @param $news
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $news)
    {
        if ($request->wantsJson()) {
            NewsService::make($news)->delete();
            return response()->noContent(Response::HTTP_NO_CONTENT);
        } else {
            return redirect()->route('user.tags.index');
        }
    }
}
