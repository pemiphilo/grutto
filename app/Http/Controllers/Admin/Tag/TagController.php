<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tag\TagStoreRequest;
use App\Http\Requests\Admin\Tag\TagUpdateRequest;
use App\Models\Tag;
use App\Services\Tag\TagService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TagController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $tags = Tag::query()->paginate();
        return view('admin.tag.index', compact('tags'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.tag.create');
    }

    /**
     * @param TagStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TagStoreRequest $request)
    {
        $inputs = $request->only('slug');
        $tagService = TagService::new();
        $tagService->create($inputs)->save();
        return redirect()->route('admin.tags.index');
    }

    /**
     * @param $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($tag)
    {
        return view('admin.tag.edit', compact('tag'));
    }

    /**
     * @param TagUpdateRequest $request
     * @param $tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TagUpdateRequest $request, $tag)
    {
        $inputs = $request->only('slug');
        TagService::make($tag)->update($inputs);
        return redirect()->route('admin.tags.index');
    }

    /**
     * @param Request $request
     * @param $tag
     * @return \Illuminate\Http\RedirectResponse|Response
     */
    public function destroy(Request $request, $tag)
    {
        if ($request->wantsJson()) {
            TagService::make($tag)->delete();
            return response()->noContent(Response::HTTP_NO_CONTENT);
        } else {
            return redirect()->route('user.tags.index');
        }
    }
}
