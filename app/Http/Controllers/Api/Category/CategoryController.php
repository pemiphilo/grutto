<?php

namespace App\Http\Controllers\Api\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\DefaultRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\NewsResource;
use App\Models\Category;
use App\Models\News;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index()
    {
        $categories = Category::with('childrenRecursive')
            ->whereNull('parent_id')
            ->get();
        return response()->json(CategoryResource::make($categories), JsonResponse::HTTP_OK);

    }

    /**
     * @param DefaultRequest $request
     * @param $category
     * @return JsonResponse
     */
    public function newsIndex(DefaultRequest $request, $category)
    {
        $news = News::withLocale($request->get('locale'))
            ->when($request->get('children'), function ($q) use ($category){
                $q->whereHas('category', function ($q) use ($category) {
                    $q->whereId($category->id)
                    ->orWhere('parent_id',$category->id);
                });
            })->when(!$request->get('children'), function ($q) use ($category) {
                $q->whereHas('category', function ($q) use ($category) {
                    $q->whereId($category->id);
                });
            })->whereNotNull('published_at')
            ->with('category')
            ->latest()
            ->paginate($request->get('count'));

        return response()->json(NewsResource::make($news), JsonResponse::HTTP_OK);
    }

}
