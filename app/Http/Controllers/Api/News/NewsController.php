<?php

namespace App\Http\Controllers\Api\News;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\DefaultRequest;
use App\Http\Resources\NewsResource;
use App\Models\News;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * @param DefaultRequest $request
     * @return JsonResponse
     */
    public function index(DefaultRequest $request)
    {
        $news = News::withLocale($request->get('locale'))
            ->whereHas('status', function ($q){
                $q->whereSlug('published');
            })->with(['category'])
            ->paginate($request->get('count'));

        return response()->json(NewsResource::make($news), JsonResponse::HTTP_OK);
    }

    /**
     * @param $news
     * @return JsonResponse
     */
    public function show($news)
    {
        return response()->json(NewsResource::make($news), JsonResponse::HTTP_OK);
    }

    /**
     * @param $locale
     * @param $slug
     * @return JsonResponse
     */
    public function showByUrl($locale, $slug)
    {
        $externalUrl = "{$locale}/article/{$slug}";
        $news = News::whereExternalUrl($externalUrl)->with(['category', 'tags'])->firstOrFail();

        return response()->json(NewsResource::make($news), JsonResponse::HTTP_OK);

    }
}
