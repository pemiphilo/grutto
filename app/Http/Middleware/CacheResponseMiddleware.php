<?php

namespace App\Http\Middleware;

use App\Services\Cache\CacheResponseService;
use Closure;

/**
 * Class CacheResponseMiddleware
 * @package App\Http\Middleware
 */
class CacheResponseMiddleware
{
    protected CacheResponseService $responseService;

    public function __construct(CacheResponseService $responseService)
    {
        $this->responseService = $responseService;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param array $args
     * @return mixed
     */
    public function handle($request, Closure $next,...$args)
    {
        $response = $next($request);

        if($this->responseService->isEnable()
            && $this->responseService->hasHeader($request->header(config('cache-response.headerTitle')))){

            if($this->responseService->isCached($request)){
                return $this->responseService->getCachedResponse($request);
            }

            $seconds = $this->keepCacheFor($args);
            $this->responseService->shouldCache($request, $response, $seconds);
        }

        return $response;

    }

    /**
     * @param array $args
     * @return int|null
     */
    protected function keepCacheFor(array $args): ?int
    {
        if (count($args) >= 1 && is_numeric($args[0])) {
            return (int) $args[0];
        }

        return config('cache-response.ttl');
    }

}
