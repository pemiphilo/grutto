<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Routing\Router;
use Vinkla\Hashids\HashidsManager;
use Illuminate\Support\Str;

/**
 * Class HashidMiddleware
 * @package App\Http\Middleware
 */
class HashidMiddleware
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var \Hashids\Hashids
     */
    protected $hashid;

    /**
     * HashidMiddleware constructor.
     * @param Router $router
     * @param HashidsManager $hashid
     */
    public function __construct(Router $router, HashidsManager $hashid)
    {
        $this->router = $router;
        $this->hashid = $hashid->connection();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->router->matched(function (RouteMatched $matched){


            $route = $matched->route;
            $request = $matched->request;

            $parameters = $route->parameters();

            foreach ($parameters as $key => $value){
                if(Str::startsWith($key, 'hashid_')){
                    $_value = $this->hashid->decode($value);
                    $_value = isset($_value[0]) ? $_value[0] : $value;
                    $route->setParameter($key, $_value);
                }
            }

            foreach ($request->all() as $key => $value){
                if(Str::startsWith($key, 'hashid_')){
                    $_value = $this->hashid->decode($value);
                    $_value = isset($_value[0]) ? $_value[0] : $value;
                    $request->offsetSet($key, $_value);
                }
            }
        });

        foreach ($request->all() as $key => $value){

            if(Str::startsWith($key, 'hashid_')){

                if(is_array($value)) {
                    $i = 0;
                    foreach ($value as $v) {
                        $_value[$i] = $this->hashid->decode($v);
                        $_value[$i] = isset($_value[$i][0]) ? $_value[$i][0] : $_value[$i];
                        ++$i;
                    }
                } else {
                    $_value = $this->hashid->decode($value);
                    $_value = isset($_value[0]) ? $_value[0] : $value;
                }
                $request->offsetSet($key, $_value);
            }
        }

        return $next($request);
    }
}
