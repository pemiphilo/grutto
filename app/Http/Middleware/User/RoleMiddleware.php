<?php

namespace App\Http\Middleware\User;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param array $role
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role)
    {
        if (Auth::guest() || !Auth::user()->hasAnyRole($role)) {
            abort(403);
        }

        return $next($request);
    }
}
