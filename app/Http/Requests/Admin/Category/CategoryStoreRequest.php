<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => [
                'required',
                'regex:/^[a-zA-Z ]+$/',
                'unique:categories,slug,NULL,id,deleted_at,NULL'
            ],
            'parent_id' => [
                'nullable',
                Rule::exists('categories', 'id')->where(function($q){
                    $q->whereNull('parent_id');
                })
            ]
        ];
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        $parent_id = app('hashids')->decode($this->get('parent_id'))[0] ?? null;
        $this->merge(['parent_id' => $parent_id]);
        return $this->all();
    }

}
