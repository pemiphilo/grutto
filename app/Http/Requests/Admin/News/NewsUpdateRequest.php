<?php

namespace App\Http\Requests\Admin\News;

use App\Models\Status;
use Illuminate\Foundation\Http\FormRequest;

class NewsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $news = $this->route()->parameters('news')['hashid_news'];
        $status = Status::whereSlug('published')->first();
        $required = $this->get('status_id') == $status->id ? 'required' : 'nullable';
        return [
            'title' => "{$required}",
            'external_url' => "{$required}|regex:/^(?!nl\/\/).+(\/article\/).*(?<![\d])$/|unique:news,external_url,{$news->id},id,deleted_at,NULL",
            'summary' => "{$required}|max:450",
            'body' => "{$required}",
            'category_id' => "{$required}|exists:categories,id",
            'status_id' => "{$required}|exists:statuses,id",
            'tags' => "array",
            'tags.*' => 'string'
        ];
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        $status_id = app('hashids')->decode($this->get('status_id'))[0] ?? null;
        $category_id = app('hashids')->decode($this->get('category_id'))[0] ?? null;
        $tags = empty(trim($this->get('tags'))) ? [] : explode(',', $this->get('tags'));
        $this->merge(['status_id' => $status_id, 'category_id' => $category_id, 'tags' => $tags]);
        return $this->all();
    }
}
