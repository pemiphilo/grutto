<?php

namespace App\Http\Requests\Admin\Tag;

use Illuminate\Foundation\Http\FormRequest;

class TagUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameters('tag')['hashid_tag']->id;

        return [
            'slug' => [
                'required',
                'regex:/^[a-zA-Z ]+$/',
                "unique:tags,slug,{$id},id"
            ],
        ];
    }
}
