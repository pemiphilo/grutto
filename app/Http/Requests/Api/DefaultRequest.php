<?php

namespace App\Http\Requests\Api;

class DefaultRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'count' => 'nullable|integer|min:1|max:100',
            'children' => 'nullable|bool',
            'locale' => 'nullable|in:en,de'
        ];
    }
}
