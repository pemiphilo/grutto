<?php

namespace App\Models;

use App\Models\Traits\HashidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HashidTrait,
        SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug'
    ];

    protected $hidden = [
        'id'
    ];

    protected $appends = [
        'title',
        'hashId'
    ];

    /*
    |--------------------------------------------------------------------------
    | Mutator
    |--------------------------------------------------------------------------
    */

    /**
     * @return string
     */
    public function getTitleAttribute(): string
    {
        return ucwords(str_replace('-', ' ', $this->attributes['slug']));
    }

    /**
     * @param $value
     */
    public function setSlugAttribute($value): void
    {
        $this->attributes['slug'] = preg_replace('/\s+/', '-',strtolower($value));
    }


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id');

    }

    /**
     * @return HasMany
     */
    public function news(): HasMany
    {
        return $this->hasMany(News::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    */

    /**
     * @return HasMany
     */
    public function childrenRecursive():HasMany
    {
        return $this->children()->with('childrenRecursive');
    }

    /**
     * @return bool
     */
    public function isParent():bool
    {
        return boolval($this->children()->count());
    }

}
