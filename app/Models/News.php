<?php

namespace App\Models;

use App\Models\Traits\HashidTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use HashidTrait,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'external_url',
        'summary',
        'body',
        'category_id',
        'status_id',
        'published_at',
        'locale'
    ];

    protected $casts = [
        'published_at' => 'datetime'
    ];

    protected $hidden = [
        'id',
        'category_id',
        'status_id',
        'locale'
    ];

    protected $appends = [
        'hashId',
    ];

    /*
    |--------------------------------------------------------------------------
    | Mutator
    |--------------------------------------------------------------------------
    */

    public function getTagsAttribute()
    {
        return implode(',', $this->tags()->pluck('slug')->toArray());
    }


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return BelongsToMany
     */
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class)->select(array('id', 'slug'));
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class)->select(array('id', 'slug'));
    }

    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class)->select(array('id', 'slug'));
    }
    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    */

    /**
     * @param Builder $builder
     * @param string|null $local
     * @return Builder
     */
    public function scopeWithLocale(Builder $builder, string $local = null): Builder
    {
        return $local ? $builder->whereLocale($local) : $builder;
    }
}
