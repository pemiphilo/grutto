<?php

namespace App\Models;

use App\Models\Traits\HashidTrait;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HashidTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug'
    ];

    protected $hidden = [
        'id'
    ];

    protected $appends = [
        'hashId'
    ];


    /*
    |--------------------------------------------------------------------------
    | Mutator
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    */

}
