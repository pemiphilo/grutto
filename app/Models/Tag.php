<?php

namespace App\Models;

use App\Models\Traits\HashidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tag extends Model
{
    use HashidTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug'
    ];

    protected $hidden = [
        'pivot'
    ];

    public  static function boot() {
        parent::boot();
        static::deleting(function (Tag $tag) {
            $tag->news()->detach();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Mutator
    |--------------------------------------------------------------------------
    */

    /**
     * @param $value
     */
    public function setSlugAttribute($value): void
    {
        $this->attributes['slug'] = preg_replace('/\s+/', ' ',strtolower($value));
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return BelongsToMany
     */
    public function news(): BelongsToMany
    {
        return $this->belongsToMany(News::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    */
}
