<?php

namespace App\Models\Traits;


use Illuminate\Database\Eloquent\ModelNotFoundException;

trait HashidTrait
{
    /**
     * @return mixed
     */
    public function getHashIdAttribute()
    {
        return $this->getHashid();
    }

    /**
     * @return mixed
     */
    public function getHashid()
    {
        return app('hashids')->connection(self::getHashidConnection())->encode($this->getKey());
    }

    /**
     * @return string
     */
    public static function getHashidConnection(): string
    {
        return defined('static::HASHID_CONNECTION') ? static::HASHID_CONNECTION: 'main';
    }

    /**
     * @return mixed
     */
    public function getHashidDecode()
    {
        return app('hashids')->connection(self::getHashidConnection())->decode($this->getHashid());
    }

    /**
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public static function findByHashid($value, $columns = ['*'])
    {
        $id = app('hashids')->connection(static::getHashidConnection())->decode($value);
        $id = isset($id[0]) ? $id[0] : null;

        if (!is_null($find = static::query()->where('id', $id)->first($columns))) {
            return $find;
        }
    }

    /**
     * Find by Name, or throw an exception.
     *
     * @param string $name .
     * @param mixed $columns The columns to return.
     *
     * @throws ModelNotFoundException if no matching User exists.
     *
     * @return static
     */

    /**
     * @param $name
     * @param array $columns
     * @return mixed
     *
     */
    public static function findByHashidOrFail($name, $columns = ['*'])
    {
        if ($find = static::findByHashid($name, $columns)) {
            return $find;
        }

        throw new ModelNotFoundException();
    }
}
