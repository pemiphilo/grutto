<?php

namespace App\Observers;

use App\Models\Category;

class CategoryObserver
{
    /**
     * Handle the category "created" event.
     *
     * @param  \App\Models\Category  $category
     * @return void
     */
    public function saving(Category $category)
    {
        //
    }

    /**
     * @param Category $category
     */
    public function deleting(Category $category)
    {
        $category->news()->delete();
        $category->children()->delete();
    }
}
