<?php

namespace App\Observers;

use App\Models\News;
use Carbon\Carbon;

class NewsObserver
{

    /**
     * @param News $news
     */
    public function saving(News $news)
    {
        if($news->isDirty('status_id')
            && $news->status()->exists() && $news->status->slug =='published' ) {
            $news->published_at = Carbon::now();
        } else if($news->isDirty('status_id')) {
            $news->published_at = NULL;
        }
        $news->locale = !$news->external_url ?: strtok($news->external_url , '/');

    }

    /**
     * @param News $news
     */
    public function deleting(News $news)
    {
        $news->tags()->detach();
    }

}
