<?php


namespace App\Services\Cache;

use App\Services\Cache\Repositories\CacheRepositoryInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CacheResponseService
{

    /**
     * @var CacheRepositoryInterface
     */
    protected CacheRepositoryInterface $cache;

    /**
     * @var cacheResponseValidationService
     */
    protected cacheResponseValidationService $cacheResponseValidationService;

    /**
     * CacheResponseService constructor.
     * @param CacheRepositoryInterface $cache
     * @param cacheResponseValidationService $cacheResponseValidationService
     */
    public function __construct(CacheRepositoryInterface $cache,
                                cacheResponseValidationService $cacheResponseValidationService)
    {
        $this->cache = $cache;
        $this->cacheResponseValidationService = $cacheResponseValidationService;
    }


    /**
     * @return bool
     */
    public function isEnable(): bool
    {
        return config('cache-response.enable');
    }

    /**
     * @param $header
     * @return bool
     */
    public function hasHeader($header): bool
    {
        return config('cache-response.header') ==  $header;
    }


    /**
     * @param Request $key
     * @return bool
     */
    public function isCached(Request $key):bool
    {
        return $this->cache->has($this->makeHash($key));
    }

    /**
     * @param Request $key
     * @return mixed
     */
    public function getCachedResponse(Request $key)
    {
        return $this->cache->get($this->makeHash($key));
    }

    /**
     * @param Request $key
     * @param Response $value
     * @param int|null $ttl
     * @return bool
     */
    public function shouldCache(Request $key, Response $value, ?int $ttl): bool
    {
        if(!$this->cacheResponseValidationService->isCacheableRequest($key)
            || !$this->cacheResponseValidationService->isCacheableResponse($value)) {
            return false;
        }
        $ttl = $ttl ?: config('cache-response.ttl');
        return $this->cache->put($this->makeHash($key), $value, $ttl);
    }


    /**
     * @param Request $request
     * @return string
     */
    protected function getFullUrl(Request $request): string
    {
        return $request->fullUrl();
    }

    /**
     * @param $key
     * @return string
     */
    protected function makeHash(Request $key): string
    {
        $key = $this->getFullUrl($key);
        return !isMd5($key) ? md5($key) : $key;
    }


}
