<?php


namespace App\Services\Cache;


use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class cacheResponseValidationService
{

    /**
     * @param Request $request
     * @return bool
     */
    public function isCacheableRequest(Request $request): bool
    {
        return $request->isMethod('get');
    }

    /**
     * @param Response $response
     * @return bool
     */
    public function isCacheableResponse(Response $response): bool
    {
        if (! $this->isCachableStatusCode($response)) {
            return false;
        }

        if (! $this->isCacheableContentType($response)) {
            return false;
        }

        return true;
    }

    /**
     * @param Response $response
     * @return bool
     */
    protected function isCachableStatusCode(Response $response): bool
    {
        if ($response->isSuccessful() || $response->isRedirection() ) {
            return true;
        }

        return false;
    }

    /**
     * @param Response $response
     * @return bool
     */
    protected function isCacheableContentType(Response $response): bool
    {
        $contentType = $response->headers->get('Content-Type', '');

        if (Str::startsWith($contentType, 'text/') || Str::contains($contentType, ['/json', '+json'])  ) {
            return true;
        }

        return false;
    }

}
