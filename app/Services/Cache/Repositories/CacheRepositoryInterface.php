<?php

namespace App\Services\Cache\Repositories;

interface CacheRepositoryInterface
{
    /**
     * @param string $key
     * @return bool
     */
    public function has(string  $key): bool;

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string  $key);

    /**
     * @param string $key
     * @param $value
     * @param int $ttl
     * @return bool
     */
    public function put(string  $key, $value, int $ttl): bool;

    /**
     * @param string $key
     * @return bool
     */
    public function forget(string  $key): bool;

    /**
     * @param string $key
     * @return bool
     */
    public function clear(): bool;
}
