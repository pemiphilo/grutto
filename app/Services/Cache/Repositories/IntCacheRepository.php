<?php


namespace App\Services\Cache\Repositories;

use Illuminate\Cache\Repository;

class IntCacheRepository implements CacheRepositoryInterface
{

    /**
     * @var Repository
     */
    protected Repository $cache;

    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @inheritDoc
     */
    public function has(string $key): bool
    {
        return $this->cache->has($key);
    }

    /**
     * @inheritDoc
     */
    public function get(string $key)
    {
        return unserialize($this->cache->get($key));
    }

    /**
     * @inheritDoc
     */
    public function put(string $key, $value, int $ttl): bool
    {
        return $this->cache->put($key, serialize($value), $ttl);
    }

    /**
     * @inheritDoc
     */
    public function forget(string $key): bool
    {
        return $this->cache->forget($key);
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        return $this->cache->clear();
    }
}
