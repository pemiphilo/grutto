<?php


namespace App\Services;

use App\Models\Status;
use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;

abstract class ModelService
{
    /**
     * @var Model|mixed
     */
    protected Model $model;

    /**
     * @var array|[]callable
     */
    protected array $beforeCallbacks = [];

    /**
     * @var array|[]callable
     */
    protected array $afterCallbacks = [];

    /**
     * @param Model $model
     * @return $this
     */
    public function model(Model $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return bool
     * @throws InvalidArgumentException
     */
    public function checkModelExists(): bool
    {
        if (!$this->model) {
            throw new InvalidArgumentException('Set argument $this->model on this service');
        }

        return true;
    }


    /**
     * @param array $data
     * @return Model|mixed
     */
    public function create(array $data = []): ?Model
    {
        $beforeCallback = function ($model) {
            if ($model->exists) {
                return false;
            }

            return true;
        };

        $this->beforeCallback($beforeCallback);

        $this->save($data);

        return $this->getModel();
    }



    /**
     * @param callable $callback
     * @return $this
     */
    public function beforeCallback(callable $callback)
    {
        $this->beforeCallbacks[] = $callback;

        return $this;
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function save($data = [])
    {
        return DB::transaction(function () use ($data) {
            $this->checkModelExists();

            $this->model->fill($data);

            // Before callback
            $beforeCallbacks = $this->beforeCallbacks;
            if (is_array($beforeCallbacks)) {
                foreach ($beforeCallbacks as $beforeCallback) {
                    if ($beforeCallback instanceof Closure) {
                        if ($beforeCallback($this->model, $this) === false) {
                            // stop continue and save model
                            return false;
                        }
                    }
                }
            }

            $save = $this->model->save();

            // After callback
            $afterCallbacks = $this->afterCallbacks;
            if (is_array($afterCallbacks)) {
                foreach ($afterCallbacks as $afterCallback) {
                    if ($afterCallback instanceof Closure) {
                        $afterCallback($this->model, $this);
                        $save = $this->model->save();
                    }
                }
            }

            return $save;
        });
    }


    /**
     * @return Model|null
     */
    public function getModel(): ?Model
    {
        return $this->model;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function update($data = []): bool
    {
        return $this->save($data);
    }

    /**
     * @param array $data
     * @return Model|mixed
     */
    public function updateOrCreate($data = [])
    {
        if (!$this->model->exists) {
            return $this->create($data);
        } else {
            return $this->update($data);
        }
    }

    /**
     * @return bool|null
     */
    public function delete(): ?bool
    {
        return DB::transaction(function () {
//            $beforeCallback = $this->beforeCallbacks;

            // Before callback
            $beforeCallbacks = $this->beforeCallbacks;
            if (is_array($beforeCallbacks)) {
                foreach ($beforeCallbacks as $beforeCallback) {
                    if ($beforeCallback instanceof Closure) {
                        if ($beforeCallback($this->model, $this) === false) {
                            return false;
                        }
                    }
                }
            }

            $delete = $this->model->delete();

            // After callback
            $afterCallbacks = $this->afterCallbacks;
            if (is_array($afterCallbacks)) {
                foreach ($afterCallbacks as $afterCallback) {
                    if ($afterCallback instanceof Closure) {
                        $afterCallback($this->model, $this);
                    }
                }
            }

            return $delete;
        });
    }

    /**
     * @param callable $callback
     * @return $this
     */
    public function afterCallback($callback)
    {
        $this->afterCallbacks[] = $callback;

        return $this;
    }

    /**
     * @param array $attributes
     * @param array $values
     * @return mixed
     */
    protected function firstOrCreate(array $attributes, array $values = [])
    {
        return DB::transaction(function () use ($attributes, $values) {
            $this->checkModelExists();

            // Before callback
            $beforeCallbacks = $this->beforeCallbacks;
            if (is_array($beforeCallbacks)) {
                foreach ($beforeCallbacks as $beforeCallback) {
                    if ($beforeCallback instanceof Closure) {
                        if ($beforeCallback($this->model, $this) === false) {
                            // stop continue and save model
                            return false;
                        }
                    }
                }
            }

            $save = $this->model->firstOrCreate($attributes, $values);

            // After callback
            $afterCallbacks = $this->afterCallbacks;
            if (is_array($afterCallbacks)) {
                foreach ($afterCallbacks as $afterCallback) {
                    if ($afterCallback instanceof Closure) {
                        $afterCallback($this->model, $this);
                        $save = $this->model->save();
                    }
                }
            }

            return $save;
        });
    }

}

