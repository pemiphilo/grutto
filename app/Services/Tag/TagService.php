<?php

namespace App\Services\Tag;

use App\Models\Tag;
use App\Observers\TagObserver;
use App\Services\ModelService;
use Illuminate\Database\Eloquent\Collection;

class TagService extends ModelService
{
    /**
     * @param  Tag  $tag
     */
    public function __construct(Tag $tag)
    {
        $this->model($tag);
    }

    /**
     * @param Tag $tag
     * @return static
     */
    public static function make(Tag $tag)
    {
        return new static($tag);
    }

    /**
     * @return static
     */
    public static function new()
    {
        return static::make(new Tag());
    }

    /**
     * @param array $attributes
     * @return Collection
     */
    public static function firstOrCreateMany(array $attributes): Collection
    {
        $data = [];
        foreach ($attributes as $attribute) {
            $data[] = static::new()->firstOrCreate($attribute);
        }
        return Collection::make($data);
    }

}
