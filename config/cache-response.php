<?php

return [

    'enable' => env('RESPONSE_CACHE_ENABLE', true),
    'ttl' => env('RESPONSE_CACHE_TTL', 86400),
    'header' => env('RESPONSE_CACHE_CUSTOM_HEADER', false),
    'headerTitle' => env('RESPONSE_CACHE_CUSTOM_HEADER_TITLE', "X-Active-Custom-Cache"),

];
