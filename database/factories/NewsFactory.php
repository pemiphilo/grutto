<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\News;
use Faker\Generator as Faker;

$factory->define(News::class, function (Faker $faker) {
    $m = $faker->randomElement(['en', 'de']);
    return [
        'title' => $faker->title,
        'summary' => $faker->text(100),
        'body' => $faker->text(200),
        'category_id' => random_int(1,4),
        'external_url' => $m . '/article/' . $faker->slug,
        'locale' => $m,
        'published_at' => \Carbon\Carbon::now(),
        'status_id' => \App\Models\Status::whereSlug('published')->first()->id,
    ];
});
