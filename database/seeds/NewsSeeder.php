<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\News::class,150)->create()->each(function ($item){
            $item->tags()->attach([random_int(1,10)]);
        });
    }
}
