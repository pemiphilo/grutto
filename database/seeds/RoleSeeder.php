<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = collect([
            'admin',
        ]);

        $roles->each(function ($item){
            \App\Models\Role::firstOrCreate(['slug' => $item]);
        });

        echo "............ RoleSeeder Done ............\n";

    }
}
