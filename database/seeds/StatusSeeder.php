<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = collect([
            'draft',
            'published',
            'scheduled'
        ]);

        $statuses->each(function ($item){
            \App\Models\Status::firstOrCreate(['slug' => $item]);
        });

        echo "............ StatusSeeder Done ............\n";
    }
}
