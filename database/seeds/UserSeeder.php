<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = collect([
            [
                'name' => 'Mr Admin',
                'email' => 'admin@gmail.com',
                'email_verified_at' => now(),
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
            ]
        ]);

        $users->each(function ($item){
            $newUser = \App\Models\User::firstOrCreate(['email' => $item['email'] ],$item);
            $newUser->roles()->sync([Role::whereSlug('admin')->first()->id]);
        });

        echo "............ UserSeeder Done ............\n";    }
}
