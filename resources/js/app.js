/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./form');

window.Vue = require('vue');


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//ADMIN
Vue.component('action-button', require('./components/button/ActionButtonComponent').default);
Vue.component('delete-button', require('./components/button/DeleteButtonComponent').default);


// SPA

import VueRouter from 'vue-router'

Vue.use(VueRouter);

import App from './views/App'
import NewsShow from "./views/NewsShow";
import CategoryNewsIndex from "./views/CategoryNewsIndex";

const router = new VueRouter({
    linkExactActiveClass: 'is-active',
    mode: 'history',
    routes: [
        {
            prop: 'categoryId',
            path: 'spa/categories/:categoryId/news',
            name: 'categories.news.index',
            component: CategoryNewsIndex,
        },
        {
            prop: 'externalUrl',
            path: 'spa/:externalUrl',
            name: 'news.show',
            component: NewsShow
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
