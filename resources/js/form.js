$(function() {
    $.validator.addMethod("slug", function(value, element) {
        return /^[A-Za-z ]+$/.test(value);
    }, "Alphabet and space only");

    $.validator.addMethod("externalUrl", function(value, element) {
        return /^(?!nl\/\/).+(\/article\/).*(?<![\d])$/.test(value);
    }, "Follow the structure {locale}/article/{slug}");

    if ($("#category").length > 0) {
        $("#category").validate({

            rules: {
                slug: {
                    required: true,
                    maxlength: 244,
                    slug: true
                },
            },
        })
    }

    if ($("#tag").length > 0) {
        $("#tag").validate({

            rules: {
                title: {
                    required: true,
                    maxlength: 244,
                    slug: true
                },
            },
        })
    }

    if ($("#news").length > 0) {
        $('#publish').click(function(){
            $("#news").validate({

                rules: {
                    title: {
                        maxlength: 244,
                        required: true
                    },
                    external_url: {
                        externalUrl: true,
                        required: true
                    },
                    summary: {
                        maxlength: 450,
                        required: true
                    },
                    body: {
                        required: true
                    },
                    category_id: {
                        required: true
                    },
                    tags: {
                        required: true
                    }
                },
            })

        });

        $('#draft').click(function(){
            $("#news").validate({

                rules: {
                    title: {
                        maxlength: 244,
                        required: false
                    },
                    external_url: {
                        externalUrl: true,
                        required: false
                    },
                    summary: {
                        maxlength: 450,
                        required: false
                    },
                    body: {
                        required: false
                    },
                    category_id: {
                        required: false
                    },
                    tags: {
                        required: false
                    }
                },
            })

        });

    }


});


