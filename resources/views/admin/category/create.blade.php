@extends('layouts.layout')

@section('content')

    <div class="card">
        <div class="card-header">{{ __('Create category') }}</div>

        <div class="card-body">
            <form action="{{ route('admin.categories.store') }}" class="form" method="post" id="category">
            @csrf
            @method('post')

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label">{{ __('Title') }}<span class="text-danger">*</span><span class="small"> {{ __('Contains alphabet and space') }}</span></label>
                        <input id="slug" type="text" class="form-control @error('slug') is-invalid @enderror"
                               name="slug" value="{{ old('slug') }}" placeholder="Business"  >
                        <div class="input">
                            @include('layouts.include.field', ['field' => 'slug'])
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6 ">
                        <label class="form-label">{{ __('Parent') }}</label>
                        <select class="form-control @error('parent_id') is-invalid @enderror" data-style="btn-default" data-live-search="true" name="parent_id">
                            <option></option>
                            @foreach($parents as $parent)
                                <option data-tokens="{{ $parent->title }}" value="{{ $parent->hashid }}"  {{ old('parent_id') == $parent->hashid ? 'selected' : '' }}>{{ $parent->title }}</option>
                            @endforeach
                        </select>
                        <div class="input">
                            @include('layouts.include.field', ['field' => 'parent_id'])
                        </div>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-success">{{ __('Submit') }}</button>
                </div>

            </form>
        </div>
    </div>
@endsection

