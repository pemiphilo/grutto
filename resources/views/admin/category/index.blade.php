@extends('layouts.layout')

@section('content')

    <div class="card">
        <div class="card-header">{{ __('Categories') }}</div>

        <div class="card-body">

            <div class="text-right mb-2">
                <a href="{{route('admin.categories.create')}}" type="submit" class="btn btn-success">{{ __('Create a category') }}</a>
            </div>
            @if(!$categories->isEmpty())
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">{{__('Title') }}</th>
                    <th scope="col">{{ __('Parent') }}</th>
                    <th scope="col">{{ __('Created at') }}</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $category->title }}</td>
                        <td>{{ optional($category->parent)->title }}</td>
                        <td>{{ $category->created_at }}</td>
                        <td>
                            <a href="{{ route('admin.categories.edit', $category->hashid) }}">{{ __('Edit') }}</a> |
                            <delete-button
                                endpoint="{{ route('admin.categories.destroy', $category->hashid) }}"
                                label="{{ __('Delete') }}"
                                text="{{ __("By deleting this category, all childrens are gonna be deleted too. Still wanna delete?") }}"
                            ></delete-button>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="p-2 mt-2">
                {{ $categories->render() }}
            </div>
            @else
                No Category Found
            @endif
        </div>
    </div>

@endsection
