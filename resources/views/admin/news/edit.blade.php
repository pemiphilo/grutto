@extends('layouts.layout')

@section('content')

    <div class="card">
        <div class="card-header">{{ __('Edit news') }}</div>

        <div class="card-body">
            <form action="{{ route('admin.news.update', $news->hashid) }}" class="form" method="post" id="news">
                @csrf
                @method('put')

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">{{ __('Title') }}</label>
                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror"
                               name="title" value="{{ old('title', $news->title) }}" placeholder="Business"  >
                        <div class="input">
                            @include('layouts.include.field', ['field' => 'title'])
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">{{ __('External Url') }}<span class="small"> {{ __('Desirable Structure: {locale}/article/{slug}') }}</span></label>
                        <input id="external_url" type="text" class="form-control @error('external_url') is-invalid @enderror"
                               name="external_url" value="{{ old('external_url', $news->external_url) }}" placeholder="de/article/cAdweCX"  >
                        <div class="input">
                            @include('layouts.include.field', ['field' => 'external_url'])
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">{{ __('Summary') }}</label>
                        <textarea id="summary" type="text" class="form-control @error('summary') is-invalid @enderror"
                                  name="summary">{{ old('summary',$news->summary) }}</textarea>
                        <div class="input">
                            @include('layouts.include.field', ['field' => 'summary'])
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">{{ __('Body') }}</label>
                        <textarea id="body" type="text" class="form-control @error('body') is-invalid @enderror"
                                  name="body">{{ old('body', $news->body) }}</textarea>
                        <div class="input">
                            @include('layouts.include.field', ['field' => 'body'])
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6 ">
                        <label class="form-label">{{ __('Category') }}</label>
                        <select class="form-control @error('category_id') is-invalid @enderror" data-style="btn-default" data-live-search="true" name="category_id">
                            <option></option>
                            @foreach($categories as $category)
                                <optgroup label="main">
                                    <option data-tokens="{{ $category->title }}" value="{{ $category->hashid }}"  {{ old('category_id', optional($news->category)->hashid) == $category->hashid ? 'selected' : '' }}>{{ $category->title }}</option>
                                    @if($category->childrenRecursive()->count())
                                        <optgroup label="subs">
                                            @foreach($category->childrenRecursive as $child)
                                                <option data-tokens="{{ $child->title }}" value="{{ $child->hashid }}"  {{ old('category_id', optional($news->category)->hashid) == $child->hashid ? 'selected' : '' }}>{{ $child->title }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endif
                                </optgroup>
                            @endforeach
                        </select>
                        <div class="input">
                            @include('layouts.include.field', ['field' => 'category_id'])
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-label">{{ __('Tags') }}<span class="small"> {{ __('Seperate tags with ,') }}</span></label>
                        <input id="tags" type="text" class="form-control @error('tags') is-invalid @enderror"
                               name="tags" value="{{ old('tags', $news->tags) }}" placeholder="Business, Job"  >
                        <div class="input">
                            @include('layouts.include.field', ['field' => 'tags'])
                        </div>
                    </div>
                </div>


                <div class="text-right">
                    <button type="submit" name="status_id" value="{{ $id = $statuses->firstWhere('slug','published')->hashId }}" id="publish" class="btn btn-success">{{ __('Publish') }}</button>
                    <button type="submit" name="status_id" value="{{ $id = $statuses->firstWhere('slug','draft')->hashId }}" id="draft" class="btn btn-primary">{{ __('Save draft') }}</button>
                </div>

            </form>
        </div>
    </div>

@endsection
