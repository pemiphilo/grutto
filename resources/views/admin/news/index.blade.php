@extends('layouts.layout')

@section('content')

    <div class="card">
        <div class="card-header">{{ __('News') }}</div>

        <div class="card-body">

            <div class="text-right mb-2">
                <a href="{{route('admin.news.create')}}" type="submit" class="btn btn-success">{{ __('Add an article') }}</a>
            </div>
            @if(!$news->isEmpty())
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">{{__('Title') }}</th>
                        <th scope="col"></th>
                        <th scope="col">{{ __('Created at') }}</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($news as $item)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $item->title }}</td>
                            <td>
                                @if($item->status->slug=='draft')
                                    @php($class='secondary')
                                @elseif($item->status->slug=='published')
                                    @php($class='success')
                                @elseif($item->status->slug=='scheduled')
                                    @php($class='primary')
                                @else
                                    @php($class='light')
                                @endif
                                <span class="badge badge-{{$class}}">{{ $item->status->slug }}</span>
                            </td>
                            <td>{{ $item->created_at }}</td>
                            <td>
                                <a href="{{ route('admin.news.edit', $item->hashid) }}">{{ __('Edit') }}</a> |
                                <delete-button
                                    endpoint="{{ route('admin.news.destroy', $item->hashid) }}"
                                    label="{{ __('Delete') }}"
                                    text="{{ __("Are you sure?") }}"
                                ></delete-button>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="p-2 mt-2">
                    {{ $news->render() }}
                </div>
            @else
                No Tag Found
            @endif
        </div>
    </div>

@endsection
