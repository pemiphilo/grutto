@extends('layouts.layout')

@section('content')

    <div class="card">
        <div class="card-header">{{ __('Edit category') }}</div>

        <div class="card-body">
            <form action="{{ route('admin.tags.update', $tag->hashid) }}" class="form" method="post" id="tag">
                @csrf
                @method('put')

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label">{{ __('Title') }}<span class="text-danger">*</span><span class="small"> {{ __('Contains alphabet and space') }}</span></label>
                        <input id="slug" type="text" class="form-control @error('slug') is-invalid @enderror"
                               name="slug" value="{{ old('slug', $tag->slug) }}" placeholder="Business"  >
                        <div class="input">
                            @include('layouts.include.field', ['field' => 'slug'])
                        </div>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-success">{{ __('Submit') }}</button>
                </div>

            </form>
        </div>
    </div>

@endsection
