@extends('layouts.layout')

@section('content')

    <div class="card">
        <div class="card-header">{{ __('Tags') }}</div>

        <div class="card-body">

            <div class="text-right mb-2">
                <a href="{{route('admin.tags.create')}}" type="submit" class="btn btn-success">{{ __('Create a tag') }}</a>
            </div>
            @if(!$tags->isEmpty())

            <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">{{__('Title') }}</th>
                        <th scope="col">{{ __('Created at') }}</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tags as $tag)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $tag->slug }}</td>
                            <td>{{ $tag->created_at }}</td>
                            <td>
                                <a href="{{ route('admin.tags.edit', $tag->hashid) }}">{{ __('Edit') }}</a> |
                                <delete-button
                                    endpoint="{{ route('admin.tags.destroy', $tag->hashid) }}"
                                    label="{{ __('Delete') }}"
                                    text="{{ __("By deleting this tag, It's gonna be removed from related news. Still wanna delete?") }}"
                                ></delete-button>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="p-2 mt-2">
                    {{ $tags->render() }}
                </div>
            @else
                No Tag Found
            @endif
        </div>
    </div>

@endsection
