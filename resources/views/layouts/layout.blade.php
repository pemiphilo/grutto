@extends('layouts.app')

@section('layout')
<div class="container">

    @auth
        <div class="row">
            <div class="col-md-4 mt-0 pt-0">
                @include('layouts.sidebar')
            </div>
            <div class="col-md-8">
                @yield('content')
            </div>
        </div>
    @endauth

    @guest
            <div class="container-fluid flex-grow-1 container-p-y col-md-6">
                @yield('content')
            </div>
    @endguest


</div>
@endsection
