
    <ul class="list-group">
        <li class="list-group-item">{{__('Articles')}}</li>
        <a class="list-group-item  pl-5" href="{{ route('admin.news.index') }}">List</a>
        <a class="list-group-item  pl-5" href="{{ route('admin.news.create') }}">Add an article</a>
        <li class="list-group-item">{{__('Categories')}}</li>
        <a class="list-group-item  pl-5" href="{{ route('admin.categories.index') }}">List</a>
        <a class="list-group-item  pl-5" href="{{ route('admin.categories.create') }}">Add a Category</a>
        <li class="list-group-item">{{__('Tags')}}</li>
        <a class="list-group-item  pl-5" href="{{ route('admin.tags.index') }}">List</a>
        <a class="list-group-item  pl-5" href="{{ route('admin.tags.create') }}">Add a tag</a>

    </ul>
