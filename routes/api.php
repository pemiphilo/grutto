<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::name('admin.')->middleware('cache_response')->group(function (){
    //Based on RestApi best practice routing
    Route::get('/news', 'Api\News\NewsController@index');
    Route::get('/news/{hashid_news}', 'Api\News\NewsController@show');

    //Based on Docs Specification
    Route::get('/article/', 'Api\News\NewsController@index');
    Route::get('{locale}/article/{external_url}', 'Api\News\NewsController@showByUrl');

    //Based on RestApi best practice routing
    Route::get('/categories', 'Api\Category\CategoryController@index');
    Route::get('/categories/{hashid_category}/news', 'Api\Category\CategoryController@newsIndex');
});



