<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admin && Auth
Auth::routes();

Route::name('admin.')->middleware('roles:admin')->group(function (){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('categories', 'Admin\Category\CategoryController')
        ->parameter('categories', 'hashid_category')
        ->except('show');

    Route::resource('tags', 'Admin\Tag\TagController')
        ->parameter('tags', 'hashid_tag')
        ->except('show');

    Route::resource('news', 'Admin\News\NewsController')
        ->parameter('news', 'hashid_news');
});


//SPA
Route::get('/{any}', 'Spa\SpaController@index')->where('any', '.*');
